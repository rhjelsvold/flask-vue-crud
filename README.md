# Developing a Single Page App with Flask and Vue.js

### Want to learn how to build this?

Check out the [post](https://testdriven.io/developing-a-single-page-app-with-flask-and-vuejs).

## MongoDB integration

@author Rune Hjelsvold

## Want to use this project?

1. Fork/Clone

1. Run the server-side Flask app in one terminal window:

    ```sh
    $ cd server
    $ python3.7 -m venv env (Windows: py -3 -m venv venv)
    $ source env/bin/activate (Windows: venv/Scripts/activate)
    (env)$ pip install -r requirements.txt
    (env)$ python app.py
    ```
    Install and start [MongoDB] (https://docs.mongodb.com/manual/administration/install-community/)

    Navigate to [http://localhost:5000/ping](http://localhost:5000/ping) and you should receive a pong!

1. Run the client-side Vue app in a different terminal window:

    ```sh
    $ cd client
    $ npm install
    $ npm run serve
    ```

    Navigate to [http://localhost:8080](http://localhost:8080)
