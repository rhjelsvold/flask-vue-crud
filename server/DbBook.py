from pymongo import MongoClient
from bson import ObjectId

class DbBook:
    """Minimal class for creating, retrieving, updating and deleting book data
       in MongoDB
    """
    def __init__(self):
        """Constructor connects to MongoDB"""
        client = MongoClient()
        self.db = client.bookDB

    def addBook(self, book):
        """Adds a book the the Book collection"""
        self.db.posts.insert_one(book)

    def getBooks(self):
        """Retrieves books returned in a list of dictionaries with keys
           id: the textual encoding of the MongoDB ObjectId
           title: the title of the book
           author: the name of the book author
           read: a flag for whether the book has been read
        """
        posts = self.db.posts.find()
        res = []
        for post in posts:
            # Jsonification of ObjectId fails - type cast to string instead
            post['id'] = str(post['_id'])
            # Remove the ObjectId so jsonify does not fail
            del post['_id']
            res.append(post)
        return res

    def modifyBook(self, id, title, author, read):
        """Modifies an existing book to the collection"""
        dbBook = self.db.posts.find_one({'_id': ObjectId(id)})
        if (dbBook):
            dbBook['title'] = title
            dbBook['author'] = author
            dbBook['read'] = read
            self.db.posts.replace_one({'_id': ObjectId(id)}, dbBook)

    def deleteBook(self, id):
        """Deletes a book from the collection"""
        self.db.posts.delete_one({'_id': ObjectId(id)})
