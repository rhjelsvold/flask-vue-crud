import uuid
import DbBook

import json

from flask import Flask, jsonify, request
from flask_cors import CORS


# configuration
DEBUG = True

# instantiate the app
app = Flask(__name__)
app.config.from_object(__name__)

# enable CORS
CORS(app, resources={r'/*': {'origins': '*'}})

dbBook = DbBook.DbBook()

# sanity check route
@app.route('/ping', methods=['GET'])
def ping_pong():
    return jsonify('pong!')

@app.route('/books', methods=['GET', 'POST'])
def all_books():
    """Two operations are called on this route depending on the method:
        GET method: All books in the collection should be retrieved
        POST method: A new book (for which there is yet no ObjecId) is to be
                     created in the database
    """
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        dbBook.addBook({
            'title': post_data.get('title'),
            'author': post_data.get('author'),
            'read': post_data.get('read')
        })
        response_object['message'] = 'Book added!'
    else:
        response_object['books'] = dbBook.getBooks()
    return jsonify(response_object)


@app.route('/books/<bookID>', methods=['PUT', 'DELETE'])
def single_book(bookID):
    """Two operations are called on this route depending on the method:
        PUT method: Data about a book should be updated in the database
        DELETE method: Data about a book is deleted from the database
    """
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        dbBook.modifyBook(bookID,
            post_data.get('title'),
            post_data.get('author'),
            post_data.get('read')
        )
        response_object['message'] = 'Successfully updated'
    if request.method == 'DELETE':
        dbBook.deleteBook(bookID)
        response_object['message'] = 'Successfully deleted'
    return jsonify(response_object)


if __name__ == '__main__':
    app.run()
